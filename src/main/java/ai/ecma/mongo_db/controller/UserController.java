package ai.ecma.mongo_db.controller;

import ai.ecma.mongo_db.document.User;
import ai.ecma.mongo_db.payload.UserDTO;
import ai.ecma.mongo_db.repos.UserRepo;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.LifecycleState;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {
    private final UserRepo userRepo;

    @PostMapping("/add")
    public String add(@RequestBody UserDTO dto) {
        User user = User.builder()
                .name(dto.getName())
                .phone(dto.getPhone())
                .build();
        User save = userRepo.save(user);
        System.out.println(save.toString());
        return "saqlandi";
    }

    @GetMapping()
    public List<User> findAll(){
        return userRepo.findAll();
    }
    @GetMapping("/{id}")
    public User getById(@PathVariable("id") String id){
        return userRepo.findById(id).get();
    }
}
