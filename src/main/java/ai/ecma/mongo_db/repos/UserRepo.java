package ai.ecma.mongo_db.repos;

import ai.ecma.mongo_db.document.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
@Component
public interface UserRepo extends MongoRepository<User,String> {
    List<User> findAllByName(String name);
}
